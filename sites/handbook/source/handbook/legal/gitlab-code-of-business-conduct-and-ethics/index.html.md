---
layout: handbook-page-toc
title: "Code of Business Conduct & Ethics"
description: "Overview of the Code of Conduct and Ethics at GitLab."
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## GITLAB INC. CODE OF BUSINESS CONDUCT AND ETHICS FOR DIRECTORS, OFFICERS, EMPLOYEES AND CONTRACTORS

As Adopted on 2020-11-07

GitLab Inc. (collectively with its subsidiaries, "**GitLab**”, “**we**”, “**our**”) is committed to the highest standards of legal and ethical business conduct and has long operated its business consistent with written operating principles and policies that reinforce this commitment. This Code of Business Conduct and Ethics (this “**Code**”) adopted by GitLab’s Board of Directors (the “Board”), acting through authority delegated to its Audit Committee (the “**Audit Committee**”), summarizes the ethical standards for all directors, officers, employees and contractors of GitLab and of its direct and indirect subsidiaries (“Team Members”) and is a reminder of the seriousness of our commitment to our values. Compliance with this Code is mandatory for every Team Member. In addition to this Code, Team Members are subject to and must comply with other policies and programs of GitLab, as applicable.

## I. INTRODUCTION

Our business is complex in terms of the geographies and cultures in which we function and the laws with which we must comply. To help Team Members understand what is expected of them and to carry out their responsibilities, we have created this Code. Responsibility for overseeing adherence to this Code shall rest with the Chief Legal Officer of GitLab (“**CLO**”), as instructed by the Audit Committee. 

This Code is not intended to be a comprehensive guide to all of our policies or to all responsibilities under law or regulation.  All Team Members are expected to be familiar with applicable laws and regulations in their area of operation, and GitLab expressly prohibits any activity by Team Members that violates any applicable law or regulation.  This Code is a guideline, or a minimum requirement, that must always be followed.

We expect each Team Member to read and become familiar with the ethical standards described in this Code. Violations of the law, our corporate policies, or this Code may result in disciplinary action, including dismissal.

## II. KEY CORPORATE VALUES

GitLab values and wishes to hold its Team Members to a code of conduct that seeks alignment with the following key corporate values:

### Collaboration & Transparency

Collaboration is one of the Company’s core values. It encompasses kindness, respect, positive intent, and fostering a supportive environment among other key beliefs.  Collaboration means that we value people equally, regardless of their seniority level in the Company, race, religion, ethnic background, gender or other personal attributes such as age, physical appearance and so on. People are to be treated with dignity, kindness and common courtesy.

Collaboration also pertains to truthfulness and transparency of motives. By being transparent, we reduce the threshold to contribution and make collaboration easier. We want to be truthful always, but most so when the other party needs to know the truth, regardless of how it may disadvantage us. We wish to be upfront about the motives that drive our goals, strategies, and behaviors.

### Results, Efficiency & Iteration

GitLab strives do what we promised to each other, our customers, users and investors. We reward, recognize and celebrate superior performance across all business disciplines. We want to excel and be the best at what we do. We will continually raise the bar on ourselves and communicate explicitly with our operating units and our employees on their performance.

We not only focus on GitLab’s results but our customers’ results. Our customers are the people who buy and use our products and services. We would not be here without our customers, we would not be a business without our customers, and we could not continue to exist without our customers.

Our daily choices must center on what's best in the aggregate for our collective group of customers. We care about working on the right things, not doing more than needed, and not duplicating work.

We endeavor to have a culture of iteration, one that is invested in continually improving our abilities to execute our stated strategies in all disciplines, in contrast to a culture that obsesses more with strategy, than execution.  When we take smaller steps, ship smaller and start with simpler features, we get feedback sooner. 

### Diversity, Inclusion and Belonging

Diversity, inclusion and belonging are principles fundamental to the success of GitLab. We aim to make a significant impact in our efforts to foster an environment where everyone can thrive. We actively chose to build and institutionalize a culture that is inclusive and supports all Team Members equally in the process of achieving their professional goals. We also work to make everyone feel welcome and to increase the participation of underrepresented minorities and nationalities in our community and company.

GitLab is committed to creating a discrimination-free work environment, and each Team Member is expected to create a respectful workplace culture that is free of harassment, intimidation, bias and unlawful discrimination. We strive to make all employment decisions based on a principle of mutual respect and dignity consistent with applicable laws.

## III. FAIR DEALING

Team Members are required to deal honestly, ethically and fairly with customers, partners, suppliers, competitors and other third parties. We:

* prohibit bribes, kickbacks or any other form of improper payment, direct or indirect, to any representative of a government, labor union, customer or supplier in order to obtain a contract, some other commercial benefit or government action;
* prohibit Team Members from accepting any bribe, kickback or improper payment from anyone;
* prohibit gifts or favors to or from any customer or supplier, except for promotional material of nominal value and gifts of modest value that are related to commonly recognized events or occasions; limit marketing and client entertainment expenditures to those that are necessary, prudent, job related and consistent with our policies;
* require clear and precise communication in our contracts, advertising, literature and other public statements and seek to eliminate misstatement of fact or misleading impressions; reflect accurately on all invoices to customers the sale price and terms of sale for products and services sold or rendered;
* protect all proprietary data our customers, partners or suppliers provide to us as reflected in our agreements with them or as compelled by law; and prohibit our representatives from otherwise taking unfair advantage of our customers, partners or suppliers, or other third parties, through manipulation, concealment, abuse of privileged information or any other unfair-dealing practice.

## IV. CONFLICTS OF INTEREST

GitLab recognizes and respects Team Members' rights to engage in outside activities that they may deem proper and desirable, provided that these activities do not impair or interfere with the performance of their duties to GitLab or their ability to act in GitLab’s best interests. However, all Team Members must avoid situations in which their personal interests may conflict, or appear to conflict, with the interests of GitLab.

It is not possible to list every situation that might give rise to a conflict of interest, but the information that follows serves as a guide, pointing out important areas where conflicts may arise. The responsibility for conduct within the letter and the spirit of this Code regarding conflicts of interest rests with each individual. It is, however, important to avoid not only any situation that is an obvious conflict of interest, but also to be aware of situations that might appear to be a conflict.

Additionally, Team Members are subject to GitLab’s [Related Party Transaction Policy](https://about.gitlab.com/handbook/legal/#gitlab-inc-related-party-transactions-policy). Team Members shall not conduct business on behalf of GitLab with a relative or a business entity with which the Team Member or a relative is associated, except where such dealings have been disclosed and approval is granted pursuant to the procedures set forth in the Related Party Transaction Policy.

### Use of GitLab Property

In the absence of prior GitLab approval, assets of GitLab should be used for legitimate business purposes and for personal purposes only to the extent allowed by GitLab policy. All Team Members have an obligation to use GitLab property efficiently and to report any theft or damage to GitLab property to appropriate GitLab management personnel.

By using any GitLab electronic equipment or systems or by accessing the Internet or any GitLab intranet using a GitLab sign-on ID or any GitLab computer equipment or systems, a Team Member acknowledges that he or she represents GitLab and agrees to comply with our [Internal Acceptable Use Policy](https://about.gitlab.com/handbook/people-group/acceptable-use-policy/).  The Internal Acceptable Use Policy specifies requirements related to the use of GitLab computing resources and data assets by Team Members so as to protect GitLab and its customers, Team Members, contractors, and other partners from harm caused by both deliberate and inadvertent misuse.

### Dealings with Partners, Suppliers and Competitors

Team Members shall select and deal with suppliers, customers, partners and other persons doing or seeking to do business with GitLab in an impartial manner, without favor or preference based upon any considerations other than the best interests of GitLab. Team Members shall not seek or accept, directly or indirectly, any payments, fees, services, or loans from any person or business entity that does or seeks to do business with GitLab. This does not, however, prohibit a Team Member from receiving compensation for outside services that GitLab permits such person to render, when such outside services will not affect the impartial discharge of such person’s duties or obligations to GitLab. In the absence of prior GitLab approval, with regard to any person or business entity that does or seeks to do business with GitLab, Team Members shall not seek or accept for themselves, or any member of their families, any gifts, entertainment, or other favors of a character that goes beyond common courtesies consistent with ethical and accepted business practices and are consistent with our internal policies.

### Interests In or Relationships with Other Companies

Team Members shall not own, directly or indirectly, a financial interest (other than the ownership of less than [1.00]% of the capital stock of a competitor whose common stock is publicly traded) in any business entity that is in competition with, or a significant financial interest in any business entity that does or seeks to do business with, GitLab except where such interest has been fully disclosed to GitLab and a determination has been made by GitLab that such interest will not influence any decision that such person might be required to make performing duties for GitLab. Team Members shall not accept a directorship or other managerial position in, or serve as a consultant or employee of, a business entity, organized for profit, that does or seeks to do business with, or is in competition with, GitLab, without receiving specific approval from their direct supervisor.

### Loans

GitLab shall not make any loans to Team Members unless the Audit Committee approves them.  No supervisor shall solicit a loan from a subordinate or accept a loan from a subordinate.  In no event shall GitLab make a loan to any member of the Board or to any officer of GitLab.

### Reporting Obligations

Team Members shall report in writing to their direct supervisor any personal ownership interest or other relationship that might affect their ability to exercise impartial, ethical business judgments in their area of responsibility. Each situation reported shall be reviewed by the Team Member’s supervisor, and the supervisor shall make a determination, with guidance from the Compliance Officer, as to whether a conflict of interest exists or may arise from such a situation. All Team Members shall give GitLab their fullest cooperation in the correction of any situation in which a conflict exists or may arise.  If the Team Member to whom the ownership interest or relationship relates is an officer of GitLab, such relationship shall be reviewed by the CLO in the manner prescribed by GitLab’s Related Party Transaction Policy.

## V. CONFIDENTIALITY AND CORPORATE ASSETS AND CORPORATE OPPORTUNITIES

Team Members are, on occasion, entrusted with GitLab confidential information and with the confidential information of GitLab suppliers, customers or other business partners. This information may include: (1) technical or scientific information about current and future services or research; (2) business or marketing plans or projections; (3) earnings and other internal financial data; (4) personnel information; (5) supply and customer lists; and (6) other non-public information that, if disclosed, might be of use to competitors, or harmful to GitLab's suppliers, customers or other business partners. This information is the property of GitLab, or the property of its suppliers, customers or business partners, and in many cases was developed at great expense. All Team Members, upon commencement of employment with GitLab, shall sign offer letters or contractor agreements that contain confidentiality provisions (the “**Confidentiality Agreement**”) provided by GitLab. Strict adherence to the Confidentiality Agreement is required of each Team Member.

Team Members shall not take for themselves, or for family members or any other entities with which they are affiliated, any opportunity of which they become aware through the use of GitLab property or information, or through their position with GitLab, and shall not use GitLab property or information, or their position with GitLab, for personal gain other than actions taken for the overall advancement of the interests of GitLab.

## VI. SPECIAL ETHICS OBLIGATIONS FOR TEAM MEMBERS WITH FINANCIAL REPORTING RESPONSIBILITIES

The Chief Executive Officer, Chief Financial Officer, and Finance Department personnel bear a special responsibility for promoting integrity throughout the organization, with responsibilities to stakeholders both inside and outside of GitLab. The Chief Executive Officer, Chief Financial Officer, and members of the Finance Department have a special role not only to adhere to these principles themselves but also to ensure that a culture exists throughout GitLab as a whole that ensures the fair, accurate, comprehensive and timely reporting of financial results. Because of this special role, the Chief Executive Officer, Chief Financial Officer, all members of the Finance Department and all other Team Members, must:

* act with honesty and integrity, avoiding actual or apparent conflicts of interest in personal and professional relationships;
* provide information that is accurate, complete, objective, timely and understandable to ensure full, fair, accurate, timely, and understandable disclosure in reports and documents that GitLab files with, or submits to, government agencies and in other public communications;
* comply with applicable governmental laws, rules and regulations, and acquire appropriate knowledge of such laws, rules and regulations relating to GitLab' duties sufficient to enable the Team Member to recognize potential dangers and to know when to seek legal advice;
* promptly report to the CLO  and/or the Chairman of the Audit Committee any conduct believed to be a violation of law or business ethics or of any provision of this Code, including any transaction or relationship that reasonably could be expected to give rise to such a conflict; and
* promote accountability to this Code among all Team Members.

## VII. COMPLIANCE WITH ALL LAWS, RULES AND REGULATIONS

GitLab will comply with all laws and governmental regulations that are applicable to its activities and expects all Team Members to obey the law. Specifically, GitLab is committed to:

* maintaining a safe and healthy work environment;
* promoting a work environment that is free from discrimination or harassment based on race, color, religion, sex, age, national origin, disability or sexual preference;
* supporting fair competition and laws prohibiting restraints of trade and other unfair trade practices;
* full compliance with applicable environmental laws; prohibiting any illegal payments, gifts, or gratuities to any government official, political party or customer; prohibiting the unauthorized use, reproduction, communication or distribution of any third party’s trade secrets, copyrighted information or confidential information; and
* complying with all applicable state and federal securities laws.

## VIII. QUESTIONS, REPORTING AND EFFECT OF VIOLATIONS

Compliance with this Code is, first and foremost, the individual responsibility of every Team Member. GitLab attempts to foster a work environment in which ethical issues and concerns may be raised and discussed with supervisors or with others without fear of retribution.

If a Team Member is aware of a suspected or actual violation of this Code by others, he, she or them has a responsibility to report it in accordance with the procedures outlined below. The Team Member and GitLab will be best served by bringing the concern into the open so that any problems can be resolved quickly, and more serious harm is prevented. GitLab will not allow any retaliation against any Team Member who acts in good faith in reporting any violation of this Code or against any person who is assisting in good faith in any investigation or process with respect to such a report. Any Team Member who participates in any such retaliation is subject to disciplinary action, including termination.

GitLab offers Team Members many ways to get answers to their questions about ethical issues and to raise any concern about a possible violation of this Code:

* Generally, each Team Member’s direct supervisor or manager (or another supervisor or manager) will be in the best position to resolve the issue quickly.
* If after raising an ethics or conduct concern the issue is not resolved, raise it with GitLab’s People Group or CLO.
* Contact EthicsPoint, GitLab’s 24-hour hotline as outlined below.
* Team Members can raise their concerns orally or in writing.

While GitLab prefers that each Team Member gives his or her name and other pertinent information when making a report because it makes the investigation and resolution of the suspected violations being reported more effective and efficient, if Team Members prefer they may also make a report anonymously.

GitLab has also engaged Navex Global to provide EthicsPoint, its comprehensive and confidential reporting tool, to provide an anonymous hotline for all Team Members. The purpose of the service is to ensure that any Team Member wishing to submit a report anonymously can do so without the fear of retribution.

EthicsPoint toll free numbers and other methods of reporting are available 24 hours a day, 7 days a week for use by Team Members.

* Website:  [http://gitlab.ethicspoint.com/](http://gitlab.ethicspoint.com/)
* Mobile intake site:  [http://m.gitlab.ethicspoint.com/](http://m.gitlab.ethicspoint.com/)
* USA Telephone: 1-833-756-0853
* All other countries: to view phone number, please see web intake site and select the country in which you are located from the drop down menu. If your country is not listed, please select the “Make a Report” link at the top of the page to access the confidential reporting tool.

Reports sent to EthicsPoint are shared with the CLO and the Chairperson of the Audit Committee.

These procedures have been established so that properly trained individuals will be made aware of and can then investigate any alleged violation of this Code. Team Members may not conduct their own investigation either before or after making a report.

Team Members who violate any law, governmental regulation or this Code will face appropriate, case-specific disciplinary action, which may include demotion or discharge. Violating this Code may also mean breaking the law, subjecting such employee and/or GitLab to criminal penalties (fines or prison sentences) or civil sanctions (damage awards or fines).

### Reporting Contacts:
* **Chief Legal Officer** – Robin Schulman  Email: [robin@gitlab.com](mailto:robin@gitlab.com)
* **Audit Committee Chairperson** – Karen Blasing  Email: [karenblasing@gmail.com](mailto:karenblasing@gmail.com)

## X. ADMINISTRATION, WAIVER AND AMENDMENT
The Audit Committee is responsible for administering the Code. It has established the standards of business conduct contained in this Code and oversees compliance. The Audit Committee has delegated day-to-day responsibility for administering and interpreting the Code to the CLO. While serving in this capacity, the CLO reports directly to the Audit Committee.

Any waiver of any provision of the Code for the benefit of a director or an executive officer (which includes without limitation, for purposes of the Code, GitLab’s principal executive, financial and accounting officers), shall be effective only if approved by the Audit Committee.

Any waivers of the Code for other Team Members may be made by the CLO or the Audit Committee.

We are committed to continuously reviewing and updating our policies and procedures. Therefore, this Code is subject to modification. Any amendment of this Code must be approved in writing by the Audit Committee and will be promptly communicated to you.
